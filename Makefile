CURRENT_DIR=$(abspath .)
INFRASTRUCTURE_DIR=${CURRENT_DIR}/infrastructure
SOURCE_DIR=${CURRENT_DIR}/bedsat

help:
	@echo "make, list of available commands"
	@echo "--------------------------------"
	@echo "to be filled, check Makefile"

test:
	pipenv run pytest bedsat/tests/
