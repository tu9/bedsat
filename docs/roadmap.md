# Release log

## v0.2

- delete units
- parse structure from json so it can be used in dynamodb
- improved exports to make it easier to use for restful

## v0.1

- sustainability score composed by energy, storage, green
- occupancy per sqm and cars depending on n. people + location
- cost and recommendation of generation + storage
- serialise / deserialise developments to and from pickle files
- json output for graphs so they can be ingested by d3
- distributable package via setup.py

# Desiderata

list of possible upcoming features:

- energy bill prediction in sustainability score
- add special appliances
- add timeseries of generation and appliances on a yearly basis
- daily timeseries for flexibility options

questions:

- how type of commercial can affect energy bill
- benchmark commercial: what type of heating + insulation, how do they affect energy bill
- battery size & dimension
