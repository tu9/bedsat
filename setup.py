import io
import re

from setuptools import find_packages
from setuptools import setup

with io.open("bedsat/version.py", "rt", encoding="utf8") as f:
    version = re.search(r'__version__ = \'(.*?)\'', f.read()).group(1)

setup(
    name="Bedsat",
    version=version,
    url="https://bedsat.open-piazza.energy/",
    project_urls={
        "Documentation": "https://bedsat.open-piazza.energy/docs/",
        "Code": "https://bitbucket.org/energyopenpiazza/bedsat/",
    },
    license="MIT",
    author="Thomas Alisi",
    author_email="thomas.alisi@unit9.com",
    maintainer="Unit9",
    maintainer_email="jobs@unit9.com",
    description="Built environment development sustainability assessment tool",

    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "networkx",
    ],
)
