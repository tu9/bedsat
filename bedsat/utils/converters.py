import json
import networkx as nx
from networkx.readwrite import json_graph

from bedsat.models.factories import ModelFactory
from .encoders import BedsatModelsEncoder


def dev_to_json_graph(development, dump=False):
    """
    returns data ready to be encoded to json and the encoder needed
    if parameter dump is true, it dumps data to a string

    :param development: an instance of Development
    :param dump: boolean, whether to dump to string
    :return: tuple, either (data, encoder) or (string, None)
    """
    data = {
        'development': json_graph.node_link_data(development._structures),
        'structures': [
            json_graph.node_link_data(s._units) for s in development.structures
        ],
    }
    if dump:
        return json.dumps(data, cls=BedsatModelsEncoder), None
    else:
        return data, BedsatModelsEncoder


def _dict_to_struct(dict):
    """
    recreates a json_graph export, typically a structure as a development
    would only have a simple graph with structure nodes

    :param dict:
    :return:
    """
    struct = None
    _units = nx.Graph()
    # temp storage to create links after 1st iteration
    nodes = {}

    # recreate nodes structure
    for n in dict['nodes']:
        node_obj = ModelFactory.from_dict(n['id'])
        _units.add_node(node_obj)
        nodes[node_obj._uid] = node_obj
        # temp storage for structures so we can add them to devs
        if node_obj.__class__.__name__ == 'Structure':
            struct = node_obj

    # recreate arcs structure
    for l in dict['links']:
        _units.add_edge(nodes[l['source']['_uid']], nodes[l['target']['_uid']])

    # all good, initialise graph
    struct._init_graph(_units)
    return struct


def json_graph_to_dev(dict):
    """
    'structures': [
        {
            'nodes': [
                {
                  'id': {
                    '_type': 'Structure',
                    '_name': 'flats',
                    '_uid': '7563d7115c5b4aa2a2fb452fc760f262'
                  }
                },
            ]
        },
    ],
    'links': [
        {
          'source': {
            '_type': 'Structure',
            '_name': 'flats',
            '_uid': '7563d7115c5b4aa2a2fb452fc760f262'
          },
          'target': {
            '_type': 'Unit',
            '_uid': 'b5aeecf80738437087cdac8b2379fadb',
            '_floor': 0,
            '_allocation_name': 'RESIDENTIAL',
            '_allocation_value': 1,
            '_surface_sqm': 100.0
          }
        },

    :param dict:
    :return:
    """
    structures = []
    for s in dict['structures']:
        structures.append(_dict_to_struct(s))

    # find the development node
    for n in dict['development']['nodes']:
        if n['id']['_type'] == 'Development':
            d = ModelFactory.from_dict(n['id'])
            # assign the structures
            for s in structures:
                d.add_structure(s)
            return d

    return None
