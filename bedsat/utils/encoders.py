from json import JSONEncoder


class BedsatModelsEncoder(JSONEncoder):
    def default(self, o):
        try:
            return o.to_dict()
        except Exception:
            # Let the base class default method handle the rest
            return JSONEncoder.default(self, o)
