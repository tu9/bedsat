from bedsat.models.factories.development import StructureFactory
from bedsat.models.energy.storage import DummyStorage, LithiumBattery


def test_dummy():
    ds = DummyStorage(0)
    assert ds.cost == 0
    assert ds.capacity == 0
    assert ds.volume == (0, 0, 0)
    assert '0kwh DummyStorage' in str(ds)


def test_lithium_cost():
    costs = {
        15: 7500,
        50: 20000,
        150: 45000,
        500: 100000,
        1000: 200000,
    }
    for capacity, cost in costs.items():
        battery = LithiumBattery(capacity)
        assert battery.cost() == cost


def test_storage_allocation():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    b1.plug_energy_sinks()
    assert b1.storage_capacity == 0
    # everybody has a battery with prob 1
    b1.allocate_random_residential_storage(1)
    assert len(list(b1.storage_nodes)) == 5
    assert b1.storage_capacity == b1.daily_energy_consumption
    b1.deallocate_storage()
    assert len(list(b1.storage_nodes)) == 0

