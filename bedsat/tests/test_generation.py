from bedsat.models.energy.generation import Photovoltaic


def test_pv_energy():
    pv = Photovoltaic(5)
    # 5mq * 0.15 panel ef * 0.8 solar ef * 5 sunshine hours
    assert pv.daily_energy == round(5 * 0.15 * 0.8 * 5.0, 3)


def test_pv_demand_to_sqm():
    for kwh in [12, 240, 4800]:
        assert round(Photovoltaic.demand_to_sqm(kwh)) in [20, 400, 8000]


def test_pv_cost():
    costs = {
        10: 2500,
        100: 14200,
        1000: 82250,
        10000: 570560,
        100000: 4274990,
        250000: 10424990,
    }
    for sqm, cost in costs.items():
        assert cost == Photovoltaic(sqm).cost()

