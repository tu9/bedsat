from bedsat.models.energy import DummyNode
from bedsat.models.energy.storage import DummyStorage
from bedsat.models.development.elements import Unit, Surface, SurfaceAllocation

from bedsat.models.serialisers import BaseSerialiserMixin, UnitSerialiserMixin, SERALIZABLE


def test_serializable_classes():
    """
    this test will fail whenever a new storage, generation or sink
    is subclassed. Which is good!

    :return:
    """
    serializable_impl = {
        'SimpleSerialiserMixin', 'UnitSerialiserMixin',
        'EnergyNode', 'DummyNode',
        'Sink', 'ResidentialSink', 'CommercialSink',
        'Generation', 'Photovoltaic',
        'Storage', 'DummyStorage', 'LithiumBattery',
        'Unit', 'Structure', 'Development'}

    xnor = serializable_impl ^ set(SERALIZABLE.keys())
    assert len(xnor) == 0


def test_energy_serialisation():
    dn = DummyNode(1)
    dn_dict = dn.to_dict()
    assert '_type' in dn_dict
    assert '_sqm' in dn_dict
    assert '_density' in dn_dict
    assert '_uid' in dn_dict


def test_energy_deserialisation():
    dn = DummyNode(1)
    dn_dict = dn.to_dict()
    dn2 = BaseSerialiserMixin.from_dict(dn_dict)
    for k in dn_dict.keys():
        assert getattr(dn, k) == getattr(dn2, k)


def test_storage_serialisation():
    ds = DummyStorage(1)
    ds_dict = ds.to_dict()
    assert '_type' in ds_dict
    assert '_capacity' in ds_dict
    assert '_uid' in ds_dict


def test_storage_deserialisation():
    ds = DummyStorage(1)
    ds_dict = ds.to_dict()
    ds2 = BaseSerialiserMixin.from_dict(ds_dict)
    for k in ds_dict.keys():
        assert getattr(ds, k) == getattr(ds2, k)


def test_unit_seralisation():
    u = Unit(Surface(100), 0, SurfaceAllocation.INDUSTRIAL, name='unit')
    u_dict = u.to_dict()
    assert '_type' in u_dict
    assert '_uid' in u_dict
    assert '_floor' in u_dict
    assert 'name' in u_dict
    assert '_allocation_name' in u_dict
    assert '_allocation_value' in u_dict
    assert '_surface_sqm' in u_dict
    u2 = UnitSerialiserMixin.from_dict(u_dict)
    for attr in ['_type', '_uid', '_floor', 'name', '_allocation']:
        assert getattr(u, attr) == getattr(u2, attr)

