import os.path
import tempfile

from .fixtures import *


def test_development_raises():
    dev = Development()
    with pytest.raises(ValueError) as e:
        dev.add_structure(None)
    assert 'structure' in str(e.value)


def test_development_name():
    dev = Development(name='test')
    assert str(dev) == 'Development: test'


def test_dev_saves_and_loads(big_commercial_building):
    dev = big_commercial_building
    floors = 10
    tot_sqm = 1500
    with tempfile.TemporaryDirectory() as tmpdirname:
        dev.save(tmpdirname)
        assert os.path.isfile(os.path.join(tmpdirname, f'{dev._uid}.json'))
        assert os.path.isfile(os.path.join(tmpdirname, f'{dev._uid}.gpickle'))
        dev_load = Development.load(os.path.join(tmpdirname, f'{dev._uid}.json'))
        assert dev._uid == dev_load._uid
        assert len(list(dev.units)) == len(list(dev_load.units))
        assert len(list(dev_load.structures)) == 1
        struct = next(dev_load.structures)
        assert next(dev.structures)._uid == struct._uid

        assert len(list(dev_load.sink_nodes)) == floors
        scale = tot_sqm / (floors * 100.0)
        for s in dev_load.sink_nodes:
            assert s.daily_energy == 25.0 * scale
        assert dev_load.daily_energy_consumption == floors * 25.0 * scale
        dev_load.unplug_energy_sinks()
        assert len(list(dev_load.sink_nodes)) == 0


def test_residential_development_sinks(std_building_with_pv):
    dev = std_building_with_pv
    floors = 5
    assert len(list(dev.structures)) == 1
    assert len(list(dev.sink_nodes)) == floors

    for s in dev.sink_nodes:
        assert s.daily_energy == 15.0
    assert dev.daily_energy_consumption == floors * 15.0

    dev.unplug_energy_sinks()
    assert len(list(dev.sink_nodes)) == 0
    assert dev.daily_energy_consumption == 0


def test_commercial_development_sinks(big_commercial_building):
    dev = big_commercial_building
    floors = 10
    tot_sqm = 1500
    assert len(list(dev.structures)) == 1

    assert len(list(dev.sink_nodes)) == floors
    scale = tot_sqm / (floors * 100.0)
    for s in dev.sink_nodes:
        assert s.daily_energy == 25.0 * scale
    assert dev.daily_energy_consumption == floors * 25.0 * scale

    dev.unplug_energy_sinks()
    assert dev.daily_energy_consumption == 0
    assert len(list(dev.sink_nodes)) == 0
