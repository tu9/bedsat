from bedsat.version import __version__
from bedsat.models.energy import DummyNode
from bedsat.models.factories.energy import EnergyNodeFactory


def test_version():
    # IF YOU NEED TO FIX THIS TEST, MAKE SURE YOU ALSO UPDATE
    # THE CHANGELOG DOCUMENTATION IN /docs/roadmap.md
    assert __version__ == '0.2'


def test_dummy_node():
    dn = DummyNode(0)
    assert '0sqm DummyNode' in str(dn)
    assert dn.daily_energy == 0


def test_energy_node_factory_silently_raises():
    assert EnergyNodeFactory._create_node(None, None) is None




