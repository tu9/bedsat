import json

from bedsat.utils.converters import dev_to_json_graph, json_graph_to_dev

from .fixtures import *


def test_json_export(big_commercial_building):
    out, cls = dev_to_json_graph(big_commercial_building)
    assert cls.__name__ == 'BedsatModelsEncoder'

    out, _ = dev_to_json_graph(big_commercial_building, dump=True)
    # simple string load, won't recreate the development
    data = json.loads(out)
    assert 'development' in data
    assert 'nodes' in data['development']
    assert len(data['development']['nodes']) == 2
    assert 'structures' in data
    assert len(data['structures']) == 1
    struct = data['structures'][0]
    assert 'nodes' in struct
    # 10 floors so 10 units with their relative sinks + dev node
    assert len(struct['nodes']) == 21


def test_json_import(std_building_with_pv):
    out, _ = dev_to_json_graph(std_building_with_pv, dump=True)
    dict = json.loads(out)
    dev = json_graph_to_dev(dict)
    assert len(list(dev.structures)) == 1
    assert std_building_with_pv._uid == dev._uid
    struct1 = next(std_building_with_pv.structures)
    struct2 = next(dev.structures)
    assert struct1._uid == struct2._uid
    assert len(list(struct1.units)) == len(list(struct2.units))
    assert struct1.tot_area == struct2.tot_area
    assert struct1.daily_energy_consumption == struct2.daily_energy_consumption
    assert struct1.storage_capacity == struct2.storage_capacity
    assert struct1.daily_energy_generation == struct2.daily_energy_generation
