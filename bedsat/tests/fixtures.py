import pytest

from bedsat.models.development import Development
from bedsat.models.development.elements import Surface, SurfaceAllocation
from bedsat.models.factories.development import StructureFactory


@pytest.fixture
def std_building_with_pv():
    data = {
        'floors': 5,
        'tot_sqm': 500,
        'name': 'flats',
    }
    b1 = StructureFactory.create_simple_building(**data)
    b1.add_unit(Surface(50), 6, SurfaceAllocation.ELECTRICITY_GENERATION_PV)

    dev = Development()
    dev.add_structure(b1)
    dev.plug_energy_sinks()
    b1.allocate_random_residential_storage(1)
    dev.allocate_generation()
    return dev


@pytest.fixture
def big_commercial_building():
    data = {
        'floors': 10,
        'tot_sqm': 1500,
        'allocation': SurfaceAllocation.COMMERCIAL,
    }
    b1 = StructureFactory.create_simple_building(**data)
    dev = Development()
    dev.add_structure(b1)
    dev.plug_energy_sinks()
    return dev
