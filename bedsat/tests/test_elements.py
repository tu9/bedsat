import pytest

from bedsat.models.development.elements import Surface, Structure, Unit
from bedsat.models.factories.development import StructureFactory
from bedsat.constants import SQFOOT2SQMETER, SurfaceAllocation
from bedsat.models.factories.energy import EnergyNodeFactory


def test_structure_raises():
    struct = Structure()

    with pytest.raises(ValueError) as e:
        struct.add_unit(None, None, None)
    assert 'surface' in str(e.value)

    with pytest.raises(ValueError) as e:
        struct.add_unit(Surface(1000), 0, None)
    assert 'allocation' in str(e.value)


def test_surface_raises():
    with pytest.raises(ValueError):
        Surface(None, None)

    with pytest.raises(ValueError):
        Surface('not a number')

    with pytest.raises(ValueError):
        Surface(None, sqf='not a number')


def test_surface_converts():
    sqm = 100
    s1 = Surface(100)
    assert s1.sqm == sqm
    assert s1.sqf == sqm / SQFOOT2SQMETER

    sqf = 1000
    s2 = Surface(sqf=sqf)
    assert s2.sqm == sqf * SQFOOT2SQMETER


def test_industrial_not_implemented():
    u = Unit(Surface(100), 0, SurfaceAllocation.INDUSTRIAL, 'factory')
    assert 'Unit: factory 100.0sqm INDUSTRIAL fl:0' == str(u)
    assert EnergyNodeFactory.create_sink(u) == None


def test_building_unit():
    name = 'big flat building'
    struct = Structure(name)
    struct.add_unit(Surface(1000), 0, SurfaceAllocation.RESIDENTIAL)
    assert f'Structure: {name}' == str(struct)
    assert len(struct._units.nodes) == 2 # root + unit
    unit = next(struct.units)
    assert unit.surface.sqm == 1000
    assert unit.floor == 0
    assert unit.allocation == SurfaceAllocation.RESIDENTIAL


def test_building_factory():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    assert len(b1._units.nodes) == 6 # root + 5 floors
    for index, unit in enumerate(b1.units):
        assert unit.surface.sqm == 100.
        assert unit.floor == index
        assert unit.allocation == SurfaceAllocation.RESIDENTIAL


def test_unit_deletion():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    b1.plug_energy_sinks()
    b1.allocate_random_residential_storage(1)
    assert len(b1._units.nodes) == 16
    # dummy delete, won't do anything
    b1.del_unit('gnagno')
    u1 = next(b1.units)
    b1.del_unit(u1._uid)
    assert len(b1._units.nodes) == 16 - 3
