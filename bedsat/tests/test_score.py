from math import isclose

from bedsat.models.development import Development
from bedsat.models.development.elements import Structure, Surface, SurfaceAllocation
from bedsat.models.factories.development import StructureFactory
from bedsat.managers.assessment import (
    SustainabilityScore, GenerationAssessment, StorageAssessment, FacilitiesAssessment)


def test_sustainability_score_keys():
    dev = Development()
    score = SustainabilityScore.assess(dev)
    keys = [
        # generation
        'energy_score', 'consumption', 'generation', 'generation_cost',
        'advised_generation_sqm', 'advised_generation_cost',
        # storage
        'storage_score', 'storage_capacity',
        'advised_storage_kwh', 'advised_storage_cost',
        # facilities
        'residential_area', 'green_area', 'parking_space', 'ev_charging_space',
        'residents_avg_n', 'cars_avg_n', 'parking_space_required',
        'green_score', 'ev_score',
        # common
        'cost_unit', 'area_unit', 'consumption_unit', 'storage_unit', 'generation_unit',
    ]
    xnor = set(keys) ^ set(score.keys())
    assert len(xnor) == 0


def test_energy_score():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    dev = Development()
    dev.add_structure(b1)
    dev.plug_energy_sinks()
    score = GenerationAssessment.assess(dev)
    # 15kwh per 100sqm
    cons = 5 * 15.0
    assert score['consumption'] == cons
    assert score['generation'] == 0

    b1.add_unit(Surface(50), 6, SurfaceAllocation.ELECTRICITY_GENERATION_PV)
    dev.allocate_generation()
    score = GenerationAssessment.assess(dev)
    # 50sqm * 0.15 panel ef * 0.8 solar ef * 5 sunshine hours
    gen = 50 * 0.15 * 0.8 * 5.0
    assert score['generation'] == gen
    assert score['energy_score'] == round(100 * gen / cons)

    assert len(list(dev.generation_nodes)) == 1
    dev.deallocate_generation()
    assert len(list(dev.generation_nodes)) == 0


def test_nil_storage_score():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    dev = Development()
    dev.add_structure(b1)
    dev.plug_energy_sinks()
    score = StorageAssessment.assess(dev)
    assert score['storage_score'] == 0
    assert score['advised_storage_kwh'] == 5 * 15.0 * 3


def test_alloc_storage_score():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    dev = Development()
    dev.add_structure(b1)
    dev.plug_energy_sinks()
    b1.allocate_random_residential_storage(1)
    score = StorageAssessment.assess(dev)
    assert score['storage_score'] == 33
    assert score['storage_capacity'] == 5 * 15.0
    # 3 - 1 means: 3x is recommended, 1x is available for all residentials
    assert score['advised_storage_kwh'] == 5 * 15.0 * (3 - 1)


def test_facilities_score():
    b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
    b1.allocate_random_residential_storage(1)

    b2 = Structure('parking + garden')
    b2.add_unit(Surface(300), -1, SurfaceAllocation.PARKING_SPACE)
    b2.add_unit(Surface(200), -1, SurfaceAllocation.PARKING_EV_CHARGING)
    b2.add_unit(Surface(500), 0, SurfaceAllocation.COMMERCIAL)
    b2.add_unit(Surface(500), 1, SurfaceAllocation.GREEN_AREA)

    dev = Development()
    dev.add_structure(b1)
    dev.add_structure(b2)

    dev.plug_energy_sinks()
    score = FacilitiesAssessment.assess(dev)
    assert score['residents_avg_n'] == 20.
    assert isclose(score['cars_avg_n'], 9.4, rel_tol=0.01)
    assert isclose(score['parking_space_required'], 9.4 * 14.4, rel_tol=0.01)
    assert isclose(score['ev_score'], 100 * 200. / score['parking_space_required'], rel_tol=0.01)
