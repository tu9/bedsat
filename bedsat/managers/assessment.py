from math import isclose

from bedsat.constants import (
    RESIDENTIAL_DENSITY, CAR_DENSITY, CAR_PARKING_SPACE_SQM, STORAGE_CONSUMPTION_MULT)

from bedsat.models.energy.generation import Photovoltaic
from bedsat.models.energy.storage import LithiumBattery


class GenerationAssessment:
    @staticmethod
    def assess(development):
        gen = development.daily_energy_generation
        cons = development.daily_energy_consumption
        score = round(100 * gen / cons) if cons is not 0 else float('NaN')
        # assuming a good generation size matches the development consumption
        adv_sqm = max(0, Photovoltaic.demand_to_sqm(cons - gen))
        cost = development.generation_cost
        return {
            'energy_score': score,
            'consumption': cons,
            'generation': gen,
            'generation_cost': round(cost, 2),
            'advised_generation_sqm': round(adv_sqm, 2),
            'advised_generation_cost': round(Photovoltaic(adv_sqm).cost(), 2),
        }


class StorageAssessment:
    @staticmethod
    def assess(development):
        # arbitrary parameter: we want 3x storage than consumption for a good score
        adv_storage = STORAGE_CONSUMPTION_MULT * development.daily_energy_consumption
        storage = development.storage_capacity
        score = round(100 * storage / adv_storage) \
            if adv_storage is not 0 else float('NaN')
        return {
            'storage_score': score,
            'storage_capacity': storage,
            'advised_storage_kwh': adv_storage - storage,
            'advised_storage_cost': (
                LithiumBattery(adv_storage - storage).cost()
                if adv_storage is not 0 else 0),
        }


class FacilitiesAssessment:
    @staticmethod
    def assess(development):
        stats = development.stats

        residents = RESIDENTIAL_DENSITY * stats['residential_area']
        cars = CAR_DENSITY * residents
        parking_space_req = CAR_PARKING_SPACE_SQM * cars

        out = {
            'residents_avg_n': round(residents, 2),
            'cars_avg_n': round(cars, 2),
            'parking_space_required': round(parking_space_req, 2),
            'green_score': round(100 * stats['residential_area'] / stats['green_area']) \
                if stats['green_area'] is not 0 else float('NaN'),
            'ev_score': round(100 * stats['ev_charging_space'] / parking_space_req) \
                if not isclose(parking_space_req, 0., rel_tol=0.01) else float('NaN'),
        }
        out.update(stats)
        return out


class SustainabilityScore:
    """
    how is the greenscore assessed?

    assuming that a structure will have
    - score 0 if all electricity comes from grid and no bonuses
    - score 100 if building is off-grid, i.e. produces at least daily consumption
    - score is like iq, can be greater than 100
    - bonuses come from allocations towards sustainability
        - (green space / building surface) ratio > threshold
        - (ev charging / parking) ratio > threshold
        - (consumption / storage) ratio > threshold

    """
    @staticmethod
    def assess(development):
        out = {
            'cost_unit': 'GBP',
            'area_unit': 'sqm',
            'consumption_unit': 'kwh',
            'storage_unit': 'kwh',
            'generation_unit': 'kwh',
        }
        for assessment_class in [
            GenerationAssessment, StorageAssessment, FacilitiesAssessment]:
            out.update(assessment_class.assess(development))
        return out
