from enum import Enum

FOOT2METER = 0.3048
SQFOOT2SQMETER = 0.09290304

# DEMOGRAPHICS

# 4 every 100sqm
RESIDENTIAL_DENSITY = 4.0 / 100
# 471 cars every 1000 people in the UK according to
# https://en.wikipedia.org/wiki/List_of_countries_by_vehicles_per_capita
CAR_DENSITY = 0.47
# 2.4 * 4.8 per car (~12) according to (then added an extra 20%):
# https://www.planningni.gov.uk/index/policy/planning_statements_and_supplementary_planning_guidance/spg_other/parking/parking_standards_considerations.htm
CAR_PARKING_SPACE_SQM = 12 * 1.2
# meh, completely random number here
STORAGE_CONSUMPTION_MULT = 3


class SurfaceAllocation(Enum):
    RESIDENTIAL = 1
    COMMERCIAL = 2
    INDUSTRIAL = 3
    GREEN_AREA = 4
    PARKING_SPACE = 5
    PARKING_EV_CHARGING = 6

    ELECTRICITY_GENERATION_PV = 100

    ELECTRICITY_STORAGE = 200
    ELECTRICITY_STORAGE_EV2G = 201
