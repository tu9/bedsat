import itertools
import uuid
from datetime import datetime
import os.path
import json

import networkx as nx

from bedsat.models.serialisers import SimpleSerialiserMixin
from .elements import Structure


class Development(SimpleSerialiserMixin):
    def __init__(self, name='', _uid=None, structures_graph=None):
        self._type = self.__class__.__name__
        self.name = str(name)
        self._uid = uuid.uuid4().hex if _uid is None else _uid
        self._init_graph(structures_graph)

    def __str__(self):
        title = self.name if len(self.name) > 0 else self._uid[5]
        return f'Development: {title}'

    def _init_graph(self, structures_graph=None):
        """
        shouldn't be used explictly unless while importing from json / dicts

        :param structures_graph:
        :return:
        """
        if isinstance(structures_graph, nx.Graph):
            self._structures = structures_graph
        else:
            self._structures = nx.Graph()
            self._structures.add_node(self)

    @property
    def structures(self):
        return filter(lambda n: isinstance(n, Structure), self._structures.nodes)

    @property
    def units(self):
        return itertools.chain(*[s.units for s in self.structures])

    @property
    def sink_nodes(self):
        return itertools.chain(*[s.sink_nodes for s in self.structures])

    @property
    def daily_energy_consumption(self):
        return sum([s.daily_energy_consumption for s in self.structures])

    @property
    def generation_nodes(self):
        return itertools.chain(*[s.generation_nodes for s in self.structures])

    @property
    def daily_energy_generation(self):
        return sum([s.daily_energy_generation for s in self.structures])

    @property
    def generation_cost(self):
        return sum([g.cost() for g in self.generation_nodes])

    @property
    def storage_capacity(self):
        return sum([s.storage_capacity for s in self.structures])

    @property
    def stats(self):
        residential_area = sum([s.residential_area for s in self.structures])
        green_area = sum([s.green_area for s in self.structures])
        parking_space = sum([s.parking_space for s in self.structures])
        ev_charging_space = sum([s.ev_charging_space for s in self.structures])

        return {
            'residential_area': residential_area,
            'green_area': green_area,
            'parking_space': parking_space,
            'ev_charging_space': ev_charging_space,
        }

    @staticmethod
    def load(path):
        with open(path, 'r') as f:
            meta = json.load(f)
        base_path = os.path.dirname(path)
        del(meta['created'])
        meta['structures_graph'] = nx.read_gpickle(
            os.path.join(base_path, f'{meta["_uid"]}.gpickle'))
        return Development(**meta)

    def save(self, base_path='.'):
        meta = {
            'name': self.name,
            '_uid': self._uid,
            'created': datetime.now().isoformat(),
        }
        with open(os.path.join(base_path, f'{self._uid}.json'), 'w') as f:
            json.dump(meta, f)
        nx.write_gpickle(self._structures, os.path.join(base_path, f'{self._uid}.gpickle'))

    def add_structure(self, structure):
        if not isinstance(structure, Structure):
            raise ValueError('structure must be of type Structure')

        self._structures.add_node(structure)
        self._structures.add_edge(self, structure)

    def unplug_energy_sinks(self):
        [s.unplug_energy_sinks() for s in self.structures]

    def plug_energy_sinks(self):
        [s.plug_energy_sinks() for s in self.structures]

    def deallocate_generation(self):
        [s.deallocate_generation() for s in self.structures]

    def allocate_generation(self):
        [s.allocate_generation() for s in self.structures]
