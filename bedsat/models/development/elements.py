import uuid
import random

import networkx as nx

from bedsat.constants import SQFOOT2SQMETER, SurfaceAllocation
from bedsat.models.factories.energy import EnergyNodeFactory
from bedsat.models.energy.sinks import Sink
from bedsat.models.energy.generation import Generation
from bedsat.models.energy.storage import Storage
from bedsat.models.serialisers import SimpleSerialiserMixin, UnitSerialiserMixin


def measure_unit_to_sqm(sqm=None, sqf=None):
    """
    This function picks the defined value for measurement unit between
    sqm and sqf and returns its numeric representation in sqm.
    Raises ValueError if both params are None or either are not convertible
    to float.

    :param sqm: square meters convertible to float
    :param sqf: square meters convertible to float
    :return: sqm as float
    """
    try:
        if sqm is not None:
            return float(sqm)
        elif sqf is not None:
            return SQFOOT2SQMETER * float(sqf)
        else:
            raise
    except Exception:
        raise ValueError('pass either sqm or sqf argument in number format')


class Surface:
    def __init__(self, sqm=None, sqf=None):
        self._sqm = measure_unit_to_sqm(sqm, sqf)

    @property
    def sqm(self):
        return self._sqm

    @property
    def sqf(self):
        return self._sqm / SQFOOT2SQMETER


class Unit(UnitSerialiserMixin):
    def __init__(self, surface, floor, allocation, name='', uid=None):
        if not isinstance(surface, Surface):
            raise ValueError('surface must be of type Surface')

        if not isinstance(allocation, SurfaceAllocation):
            raise ValueError(f'allocation must be one of {[a for a in SurfaceAllocation]}')

        self._type = self.__class__.__name__
        self._surface = surface
        self._floor = floor
        self._allocation = allocation
        self.name = str(name)
        self._uid = uuid.uuid4().hex if uid is None else uid

    def __str__(self):
        title = self.name if len(self.name) > 0 else self._uid[5]
        return f'Unit: {title} {self._surface.sqm}sqm {self._allocation.name} fl:{self._floor}'

    @property
    def surface(self):
        return self._surface

    @property
    def floor(self):
        return self._floor

    @property
    def allocation(self):
        return self._allocation


class Structure(SimpleSerialiserMixin):
    """
    can be a building, or a parking space with pv, or a green space.
    Basically any combination of units
    """
    def __init__(self, name='', _uid=None, units_graph=None):
        self._type = self.__class__.__name__
        self.name = str(name)
        self._uid = uuid.uuid4().hex if _uid is None else _uid
        self._init_graph(units_graph)

    def __str__(self):
        title = self.name if len(self.name) > 0 else self._uid[5]
        return f'Structure: {title}'

    def _init_graph(self, units_graph=None):
        """
        shouldn't be used explictly unless while importing from json / dicts

        :param units_graph:
        :return:
        """
        if isinstance(units_graph, nx.Graph):
            self._units = units_graph
        else:
            self._units = nx.Graph()
            self._units.add_node(self)

    @property
    def units(self):
        return filter(lambda n: isinstance(n, Unit), self._units.nodes)

    @property
    def residential_units(self):
        return filter(
            lambda u: u.allocation == SurfaceAllocation.RESIDENTIAL,
            self.units)

    @property
    def sink_nodes(self):
        return filter(lambda n: isinstance(n, Sink), self._units.nodes)

    @property
    def generation_nodes(self):
        return filter(lambda n: isinstance(n, Generation), self._units.nodes)

    @property
    def storage_nodes(self):
        return filter(lambda n: isinstance(n, Storage), self._units.nodes)

    @property
    def daily_energy_consumption(self):
        return sum([s.daily_energy for s in self.sink_nodes])

    @property
    def daily_energy_generation(self):
        return sum([g.daily_energy for g in self.generation_nodes])

    @property
    def storage_capacity(self):
        return sum([s.capacity for s in self.storage_nodes])

    @property
    def tot_area(self):
        return sum([u.surface.sqm for u in self.units])

    @property
    def residential_area(self):
        return sum([u.surface.sqm for u in self.residential_units])

    @property
    def green_area(self):
        return sum([u.surface.sqm for u in filter(
            lambda u: u.allocation == SurfaceAllocation.GREEN_AREA,
            self.units)])

    @property
    def parking_space(self):
        return sum([u.surface.sqm for u in filter(
            lambda u: u.allocation == SurfaceAllocation.PARKING_SPACE,
            self.units)])

    @property
    def ev_charging_space(self):
        return sum([u.surface.sqm for u in filter(
            lambda u: u.allocation == SurfaceAllocation.PARKING_EV_CHARGING,
            self.units)])

    def add_unit(self, surface, floor, allocation):
        unit = Unit(surface, floor, allocation)
        self._units.add_node(unit)
        self._units.add_edge(self, unit)

    def del_unit(self, unit_uid):
        try:
            unit = next(filter(lambda u: u._uid == unit_uid, self.units))
        except StopIteration:
            return
        else:
            for neighbor in list(filter(lambda n: not isinstance(n, Structure), self._units.neighbors(unit))):
                self._units.remove_node(neighbor)
            self._units.remove_node(unit)

    def unplug_energy_sinks(self):
        for n in list(self.sink_nodes):
            self._units.remove_node(n)

    def _allocate_nodes(self, factory):
        nodes = []
        # self.units is a generator, we want to avoid creating a static list
        # in case it's too big, so we store new nodes in a temporary a list
        # to avoid changing the iterator's content while accessing it
        for u in self.units:
            n = factory(u)
            if n is not None:
                nodes.append((u, n))

        # ok, now we can add the nodes to the graph
        for u, n in nodes:
            self._units.add_node(n)
            self._units.add_edge(u, n)

    def plug_energy_sinks(self):
        self.unplug_energy_sinks()
        self._allocate_nodes(EnergyNodeFactory.create_sink)

    def deallocate_generation(self):
        for n in list(self.generation_nodes):
            self._units.remove_node(n)

    def allocate_generation(self):
        self.deallocate_generation()
        self._allocate_nodes(EnergyNodeFactory.create_generation)

    def deallocate_storage(self):
        for n in list(self.storage_nodes):
            self._units.remove_node(n)

    def allocate_random_residential_storage(self, probability=0.5):
        nodes = []
        for r in self.residential_units:
            if random.random() < probability:
                nodes.append((r, EnergyNodeFactory.create_storage_matching_sink(self._units, r)))

        # ok, now we can add the nodes to the graph
        for u, n in nodes:
            self._units.add_node(n)
            self._units.add_edge(u, n)
