SERALIZABLE = {}


class BaseSerialiserMixin:
    def __init_subclass__(cls, **kwargs):
        if cls not in SERALIZABLE.values():
            SERALIZABLE[cls.__name__] = cls
        super().__init_subclass__(**kwargs)

    def to_dict(self):
        return self.__dict__

    @staticmethod
    def from_dict(dict):
        _type = dict.pop('_type')
        cls = SERALIZABLE[_type]
        obj = cls(**dict)
        return obj


class SimpleSerialiserMixin(BaseSerialiserMixin):
    def to_dict(self):
        return {
            '_type': self.__class__.__name__,
            'name': getattr(self, 'name'),
            '_uid': getattr(self, '_uid'),
        }


class UnitSerialiserMixin(BaseSerialiserMixin):
    def to_dict(self):
        return {
            '_type': self.__class__.__name__,
            '_uid': getattr(self, '_uid'),
            '_floor': getattr(self, '_floor'),
            'name': getattr(self, 'name'),
            '_allocation_name': getattr(getattr(self, '_allocation'), 'name'),
            '_allocation_value': getattr(getattr(self, '_allocation'), 'value'),
            '_surface_sqm': getattr(getattr(self, '_surface'), '_sqm'),
        }

    @staticmethod
    def from_dict(dict):
        from bedsat.models.development.elements import Surface, SurfaceAllocation
        cls = SERALIZABLE['Unit']
        return cls(
            surface=Surface(dict['_surface_sqm']),
            floor=dict['_floor'],
            allocation=SurfaceAllocation[dict['_allocation_name']],
            name=dict['name'],
            uid=dict['_uid'])

