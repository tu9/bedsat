from bedsat.models.serialisers import SERALIZABLE


class ModelFactory:
    @staticmethod
    def from_dict(dict):
        cls = SERALIZABLE[dict['_type']]
        return cls.from_dict(dict)
