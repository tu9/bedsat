from bedsat.constants import SurfaceAllocation

from bedsat.models.development.elements import measure_unit_to_sqm, Surface, Structure


class StructureFactory:
    @staticmethod
    def create_simple_building(
        floors=1, tot_sqm=None, tot_sqf=None, allocation=SurfaceAllocation.RESIDENTIAL, name=''):
        sqm = measure_unit_to_sqm(tot_sqm, tot_sqf)
        sqm_per_floor = sqm / float(floors)
        out = Structure(name)
        for i in range(int(floors)):
            out.add_unit(Surface(sqm_per_floor), i, allocation)
        return out
