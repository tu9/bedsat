import logging

from bedsat.constants import SurfaceAllocation
from bedsat.models.energy.sinks import Sink, ResidentialSink, CommercialSink
from bedsat.models.energy.generation import Photovoltaic
from bedsat.models.energy.storage import LithiumBattery


logger = logging.getLogger('built-assess')


class EnergyNodeFactory:
    """
    creates an instance of an energy node depending on unit type argument
    """
    @staticmethod
    def create_sink(unit):
        node_mapper = {
            SurfaceAllocation.RESIDENTIAL: ResidentialSink,
            SurfaceAllocation.COMMERCIAL: CommercialSink,
        }
        return EnergyNodeFactory._create_node(unit, node_mapper)

    @staticmethod
    def create_generation(unit):
        node_mapper = {
            SurfaceAllocation.ELECTRICITY_GENERATION_PV: Photovoltaic,
        }
        return EnergyNodeFactory._create_node(unit, node_mapper)

    @staticmethod
    def create_storage_matching_sink(graph, unit):
        sinks = filter(lambda n: isinstance(n, Sink), graph.neighbors(unit))
        tot_energy = sum([s.daily_energy for s in sinks])
        return LithiumBattery(tot_energy)

    @staticmethod
    def _create_node(unit, node_mapper):
        try:
            mapper_class = node_mapper.get(unit.allocation, None)
            if mapper_class is not None:
                return mapper_class(unit.surface.sqm)
        except Exception as e:
            # if param unit is not of type Unit
            logger.debug(f'create node caught: {str(e)}')
        return None
