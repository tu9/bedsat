from . import EnergyNode


class Generation(EnergyNode):
    pass


class Photovoltaic(Generation):
    """
    useful links:
    - https://medium.com/the-mission/what-size-of-a-solar-system-do-you-need-and-how-to-pay-or-it-e59b70917502
    - https://pvwatts.nrel.gov/
    """
    sun_sqm_instant_power = 1 #kw
    panel_efficiency = 0.15
    sunshine_hours_daily = 5.0
    sunshine_efficiency = 0.8

    @property
    def adjusted_sunshine(self):
        """
        this should take into consideration:
        - coordinates to retrieve weather and yearly sunshine data
        - period of the year
        - tilt & zoom of panels affecting efficiency, if installation is not tracking

        :return: daily avg of sunshine on yearly basis
        """
        return self.sunshine_hours_daily * self.sunshine_efficiency

    def _compute_energy(self):
        return (
            self._sqm *
            self.sun_sqm_instant_power *
            self.panel_efficiency *
            self.adjusted_sunshine)

    @staticmethod
    def demand_to_sqm(kwh):
        """
        overall efficiency = 0.15 * 0.8 = 0.12
        peak per sqm = 0.12 * 5 = 0.6 kwh
        6kwp household = 10sqm = 6kwh / 0.6kwh/sqm

        :param kwh:
        :return:
        """
        kwh_per_mq = Photovoltaic.panel_efficiency * \
                     Photovoltaic.sunshine_hours_daily * \
                     Photovoltaic.sunshine_efficiency
        return kwh / kwh_per_mq

    def cost(self):
        """
        10sqm = installation 1500 + panels 100 * sqm
        100sqm = installation 5000 + panels 80 * sqm
        1000sqm = installation 15000 + panels 60 * sqm
        10000sqm = installation 40000 + panels 50 * sqm

        :param sqm:
        :return:
        """
        # sqm, installation cost, panel cost per sqm
        bands = [
            (10, 1500, 100),
            (100, 5000, 80),
            (1000, 15000, 60),
            (10000, 40000, 50),
            (100000, 100000, 40),
        ]
        sqm = self._sqm
        band = 0
        tot_cost = 0
        while sqm > 0:
            band = band + 1 if band < len(bands) else band
            limit, installation, cost_sqm = bands[band - 1]
            actual = min(limit, sqm)
            sqm = sqm - limit
            segm_cost = (installation * actual / limit) + (actual * cost_sqm)
            tot_cost += segm_cost
        return tot_cost
