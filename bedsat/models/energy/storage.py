import uuid
from abc import ABC

from bedsat.models.serialisers import BaseSerialiserMixin


class Storage(ABC, BaseSerialiserMixin):
    def __init__(self, _capacity, _uid=None):
        self._type = self.__class__.__name__
        self._capacity = _capacity
        self._uid = uuid.uuid4().hex if _uid is None else _uid

    def __str__(self):
        return f'{self.capacity}kwh {self._type} {self._uid[5]}'

    @property
    def capacity(self):
        return self._capacity

    @property
    def cost(self):
        return 0

    @property
    def volume(self):
        """
        returns a tuple w, h, d in meters

        :return: (w, h, d) tuple
        """
        return (0, 0, 0)


class DummyStorage(Storage):
    pass


class LithiumBattery(Storage):
    def cost(self):
        bands = [
            (15, 500),
            (50, 400),
            (150, 300),
            (500, 200),
        ]
        for cap, cost_kwh in bands:
            if self.capacity <= cap:
                return self.capacity * cost_kwh
        return self.capacity * bands[-1][1]
