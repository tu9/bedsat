import uuid
from abc import ABC

from bedsat.models.serialisers import BaseSerialiserMixin


class EnergyNode(ABC, BaseSerialiserMixin):
    def __init__(self, _sqm, _density=1.0, _uid=None):
        self._type = self.__class__.__name__
        self._sqm = float(_sqm)
        self._density = _density
        self._uid = uuid.uuid4().hex if _uid is None else _uid

    def __str__(self):
        return f'{self._sqm}sqm {self._type} {self._uid[5]}'

    @property
    def daily_energy(self):
        return self._compute_energy()

    def _compute_energy(self):
        return 0


class DummyNode(EnergyNode):
    pass
