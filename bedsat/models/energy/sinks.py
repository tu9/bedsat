from . import EnergyNode


class Sink(EnergyNode):
    avg_consumption = 0
    avg_surface = 100.0

    def _compute_energy(self):
        scale = self._sqm / self.avg_surface
        return self.avg_consumption * scale * self._density


class ResidentialSink(Sink):
    """15kwh / 100mq is pretty much the size of the battery that Tesla recommends
    for a household without special appliances like A/C
    """
    avg_consumption = 15.0
    avg_surface = 100.0


class CommercialSink(Sink):
    """no idea if 25kwh / 100mq makes sense, and it should depend on the type of biz
    """
    avg_consumption = 25.0
    avg_surface = 100.0

