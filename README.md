# Built Environment Development Sustainability Assessment Tool (BEDSAT)

a prototype tool that aims at giving a "green score" to an existing or 
new development in the built environment.

## Requirements

python 3.6 with pipenv

## Run and test

to run tests:

```bash
pipenv install --dev
make test
```

here is an example that creates structures in a development and allocate energy nodes:

```python
from bedsat.models.development import Development
from bedsat.models.development.elements import  Surface, SurfaceAllocation
from bedsat.models.factories.factories import StructureFactory
from bedsat.managers.assessment import SustainabilityScore

# create a building and assign to development
b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500)
b1.add_unit(Surface(50), 6, SurfaceAllocation.ELECTRICITY_GENERATION_PV)

# create development and assign resources
dev = Development()
dev.add_structure(b1)
# plug all energy sinks, i.e. create sink nodes corresponding to surface allocation
dev.plug_energy_sinks()
# create storage nodes: order is important! 
# automatic allocation of batteries depends on sinks
b1.allocate_random_residential_storage(0.7)
# allocate generation nodes
dev.allocate_generation()

# evaluate score: this shows all scores aggregated into a single assessment
score = SustainabilityScore.assess(dev)
```

and a more complete example with multiple structures with drawings of the development graphs:

```python
import networkx as nx
import matplotlib.pyplot as plt

from bedsat.models.development import Development
from bedsat.models.development.elements import  Surface, SurfaceAllocation
from bedsat.models.factories.factories import StructureFactory
from bedsat.managers.assessment import SustainabilityScore
from bedsat.utils.exporters import dev_to_json_graph

# create building 1
b1 = StructureFactory.create_simple_building(floors=5, tot_sqm=500, name='flats')
b1.add_unit(Surface(500), 6, SurfaceAllocation.ELECTRICITY_GENERATION_PV)

# create building 2
b2 = StructureFactory.create_simple_building(
    floors=1, tot_sqm=500, allocation=SurfaceAllocation.COMMERCIAL, name='cool shops')
b2.add_unit(Surface(200), -1, SurfaceAllocation.PARKING_EV_CHARGING)
b2.add_unit(Surface(300), -1, SurfaceAllocation.PARKING_SPACE)
b2.add_unit(Surface(500), 1, SurfaceAllocation.GREEN_AREA)

# create developmnent and add buildings
d = Development('multi purpose')
d.add_structure(b1)
d.add_structure(b2)

# create sinks / storage / generation nodes
d.plug_energy_sinks()
b1.allocate_random_residential_storage(0.7)
d.allocate_generation()

# plot graphs
plt.subplot(131)
nx.draw(d._structures, with_labels=True)
plt.subplot(132)
nx.draw(b1._units, with_labels=True)
plt.subplot(133)
nx.draw(b2._units, with_labels=True)

# score
score = SustainabilityScore.assess(d)
print(score)

# json graph
json_graph, _ = dev_to_json_graph(d, dump=True)
print(json_graph)
```

which generates the following output for `score` (might slightly change subject to evolving codebase):

```python
{
  'cost_unit': 'GBP', 
  'area_unit': 'sqm', 
  'consumption_unit': 'kwh', 
  'storage_unit': 'kwh', 
  'generation_unit': 'kwh', 
  'energy_score': 150, 
  'consumption': 200.0, 
  'generation': 300.0, 
  'generation_cost': 44750.0, 
  'advised_generation_sqm': 0, 
  'advised_generation_cost': 0, 
  'storage_score': 10, 
  'storage_capacity': 60.0, 
  'advised_storage_kwh': 540.0, 
  'advised_storage_cost': 108000.0, 
  'residents_avg_n': 20.0, 
  'cars_avg_n': 9.4, 
  'parking_space_required': 135.36, 
  'green_score': 100, 
  'ev_score': 148, 
  'residential_area': 500.0, 
  'green_area': 500.0, 
  'parking_space': 300.0, 
  'ev_charging_space': 200.0
}
```

and `plot`:

![development](./docs/img/bedsat_demo.png)
